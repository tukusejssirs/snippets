// JSON.stringify replacer function to convert UNIX epoch to ISO string
const a = {b: new Date().valueOf(), c: 1000}
const dateProperty = 'b'
JSON.stringify(a, (i, v) => i === dateProperty ? new Date(v).toISOString() : v, 2)
