function doIWorkThisDay(date, dateType){
  var weekDay = getWeekDayNum(date);  // Sun = 0; Sat = 6
  var doIWork = 0;  // 1 = true, 0 = false, 2 = poldna dovolenky
  
  // 0,5d  poldna dovolenky
  // 0.5d  poldna dovolenky
  // 60%   prekazka na strane zamestnavatela (prva zmena v mesiaci: 60 %)
  // 65%   prekazka na strane zamestnavatela (ostatne zmeny v mesiac: 65 %)
  // cz    celozavodna dovolenka
  // d     dovolenka
  // n     nocna zmena
  // p     poobedna zmena
  // r     ranna zmena
  // rc    darovanie krvi (Red Cross)
  // sp    odpracovana sobota
  // sv    sviatok
  // svp   odpracovany sviatok

  switch(dateType){
    case "svp" :
    case "sp" :
      doIWork = 1;
      break;
    case "sv":
    case "d" :
    case "rc" :
    case "cz" :
    case "60%" :
    case "65%" :
    case "cz" :
    case "rc" :
      doIWork = 0;
      break;
    case "0,5d" :
    case "0.5d" :
      doIWork = 2;
      break;
    default :
      switch(weekDay){
        case 0 :
        case 6 :
          doIWork = 0;
          break;
        case 1 :
        case 2 :
        case 3 :
        case 4 :
        case 5 :
          doIWork = 1;
          break;
      }
      break;
  }
//  Logger.log(doIWork);
//  Logger.log(weekDay);
//  Logger.log(dateType);
  return doIWork;
}