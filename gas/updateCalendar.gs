function updateCalendar(){  // input: ss, calName, calId
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("workShiftsSimple");  // TODO: from input
  var data = sheet.getRange("A2:F").getValues();
  var calName;  // TODO: from input  // sheet name containing data must have the cal name of updated cal
  //var calId = "6h6roj004476afdp51pt0nigtg@group.calendar.google.com";  // TODO: from input  // o_work_shifts
  var calId = "r9vlr3fmcd73iuesitike2gch4@group.calendar.google.com";  // work_shifts_test
  var cal = CalendarApp.getCalendarById(calId);  // workShift cal
  var desc, date, busDep, busArr, busDepSplit, busArrSplit, startTime, endTime, event, eventId, actionType, eventIdExists=0;
  var i, j=0;
  
  Logger.log("data[0][0] = " + data[0][0]);
  Logger.log("data[0][1] = " + data[0][1]);
  Logger.log("data[0][2] = " + data[0][2]);
  Logger.log("data[0][3] = " + data[0][3]);
  Logger.log("data[0][4] = " + data[0][4]);
  Logger.log("data[0][5] = " + data[0][5]);
 
  // Check if event should be [c]reated, [u]pdated, or [d]eleted
  for(i=0; i<data.length; i++){
    eventId = data[i][5];
    actionType = data[i][1];
    Logger.log("data[" + i + "][1] = " + data[i][1]);
    Logger.log("eventId = " + eventId);
    Logger.log("actionType = " + actionType);
    
    switch(eventId){
      case "" :  // If there's no eventId
        switch(actionType){
          case "c" :
            // Create the event
            date = data[i][0];
            desc = data[i][2];
            busDep = data[i][3];
            busArr = data[i][4];
            Logger.log("busDep = " + busDep);
            Logger.log("busArr = " + busArr);
            createEvent(date, desc, busDep, busArr, cal);
            break;
          case "u" :
            // Ask user if the event should be created with new ID or the row deleted or left alone
            break;
          case "d" :
            // Ask user if the event should the row be deleted or left alone
            break;
        }
        break;
      default :  // If there' is an eventId
        // First check if the eventId is in cal (if it exists)
        cal.getEventSeriesById(eventId);  // TODO using https://stackoverflow.com/a/15790894/3408342
        
        if(eventIdExists == 1){  // If eventId exists
          switch(actionType){
            case "c" :
              busDep = data[i][3];
              busArr = data[i][4];
              createEvent(date, desc, busDep, busArr, cal);
              break;
            case "u" :
              break;
            case "d" :
              break;
          }
        }else{  // If eventId does not exist
        }
    }
    
    if(data[i][2] === "x"){
    busDep = data[i][3];
    busArr = data[i][4];
      createEvent(date, desc, busDep, busArr, cal);
    }
  }
  sheet.getRange("E2:E").clear();  // TODO: clear only the cell needed
}