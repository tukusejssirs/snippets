function isCZ(date) {
  var date = date.getTime();
  var cz = [new Date(2017, 6, 24).getTime(),
            new Date(2017, 6, 25).getTime(),
            new Date(2017, 6, 26).getTime(),
            new Date(2017, 6, 27).getTime(),
            new Date(2017, 6, 28).getTime(),
            new Date(2017, 6, 29).getTime(),
            new Date(2017, 6, 30).getTime(),
            new Date(2017, 6, 31).getTime(),
            new Date(2017, 7, 1).getTime(),
            new Date(2017, 7, 2).getTime(),
            new Date(2017, 7, 3).getTime(),
            new Date(2017, 7, 4).getTime(),
            new Date(2017, 7, 5).getTime(),
            new Date(2017, 7, 6).getTime(),
            new Date(2017, 7, 7).getTime(),
            new Date(2017, 7, 8).getTime(),
            new Date(2017, 7, 9).getTime(),
            new Date(2018, 0, 2).getTime(),
            new Date(2018, 6, 23).getTime(),
            new Date(2018, 6, 24).getTime(),
            new Date(2018, 6, 25).getTime(),
            new Date(2018, 6, 26).getTime(),
            new Date(2018, 6, 27).getTime(),
            new Date(2018, 6, 30).getTime(),
            new Date(2018, 6, 31).getTime(),
            new Date(2018, 7, 1).getTime(),
            new Date(2018, 7, 2).getTime(),
            new Date(2018, 7, 3).getTime()];

  for(var i=0; i < cz.length; i++){
    if(date == cz[i]){
      return 1;  // It is a cz
    }
  }
  return 0;  // It is not a cz
}