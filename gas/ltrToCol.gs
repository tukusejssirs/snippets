// This function converts the column letter to its number equivalent (starting with zero).

// author:  AdamL (https://stackoverflow.com/users/1373663/adaml); modified by Tukusej's Sirs
// date:    20 January 2014
// version: 1.0

// src: https://stackoverflow.com/a/21231012/3408342

function ltrToCol(letter){
  var column = 0, length = letter.length;
//  var column = 0, length = letter.length;
  for (var i = 0; i < length; i++){
    column += (letter.toUpperCase().charCodeAt(i) - 64) * Math.pow(26, length - i - 1);
  }
  return column;
}