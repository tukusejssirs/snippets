/**
This function is able to add or substract any number of days to/from a date.
To substract days set days to a negative number.

src: https://stackoverflow.com/a/19691491/3408342 [30 Dec 2017]
*/

function addDaysToDate(date, days){
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}