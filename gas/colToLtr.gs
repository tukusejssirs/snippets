// This function converts the column number (starting with zero) to its letter equivalent.

// author:  AdamL (https://stackoverflow.com/users/1373663/adaml)
// date:    20 January 2014
// version: 1.0

// src: https://stackoverflow.com/a/21231012/3408342

function colToLtr(column){
  var temp, letter = '';
  while (column > 0){
    temp = (column - 1) % 26;
    letter = String.fromCharCode(temp + 65) + letter;
    column = (column - temp - 1) / 26;
  }
  return letter;
}