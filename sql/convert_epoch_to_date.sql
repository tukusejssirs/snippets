# Convert UNIX Epoch to date
# Note: `1000.0` is required (instead of `1000`), otherwise the decimal fraction would dropped.
select to_timestamp(1234567890123 / 1000.0);