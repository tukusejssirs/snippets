-- Get how many connections are used and how many connections are available
select max_conn, used, res_for_super, max_conn - used - res_for_super res_for_normal from
	(select count( * ) used from pg_stat_activity) t1,
	(select setting::int res_for_super from pg_settings where name = $$superuser_reserved_connections$$) t2, (select setting::int max_conn from pg_settings where name = $$max_connections$$) t3;

-- Get how many connections are used
select count(*) used from pg_stat_activity;