-- Get UNIX Epoch from date string
select extract(epoch from '2022-01-22 15:00:00.000+00'::timestamp) * 1000;