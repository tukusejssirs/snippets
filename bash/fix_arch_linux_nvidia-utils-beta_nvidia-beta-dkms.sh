#!/usr/bin/env bash

# Fix installation of updates of `nvidia-utils-beta` and `nvidia-beta-dkms`


# Running `yay -Syu` while it contains updates of `nvidia-utils-beta nvidia-beta-dkms` (both packages should be clean-built) fails with this error:
# looking for conflicting packages...
# error: failed to prepare transaction (could not satisfy dependencies)
# :: installing nvidia-utils-beta (515.57-1) breaks dependency 'nvidia-utils-beta=515.48.07' required by nvidia-beta-dkms
#  -> exit status 1

# Running `sudo pacman -U ~/.cache/yay/nvidia-utils-beta/ ~/.cache/yay/nvidia-beta-dkms` fails with this error:
# error: could not open file /home/ts/.cache/yay/nvidia-utils-beta/: Error reading fd 7
# error: '/home/ts/.cache/yay/nvidia-utils-beta/': cannot open package file
# error: could not open file /home/ts/.cache/yay/nvidia-beta-dkms: Error reading fd 7
# error: '/home/ts/.cache/yay/nvidia-beta-dkms': cannot open package file

# Fix by removing and reinstalling the packages
yay -R nvidia-utils-beta nvidia-beta-dkms
yay -S nvidia-utils-beta nvidia-beta-dkms

# Just to make sure everything is updated
yay -Syu