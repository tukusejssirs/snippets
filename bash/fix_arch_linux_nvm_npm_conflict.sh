#!/usr/bin/env bash

# Fix `npm` and `nvm` conflict (or something in between)


# Move old global `node_modules`
sudo mv /usr/lib/node_modules{,.bak}

# Reinstall system `npm`
yay -S npm

# Remove packages installed by `npm` system package
sudo rm -rf /usr/lib/node_modules.bak/{node-gyp,nopt,npm}

# Move previously globally installed NPM packages into newly installed global `node_modules`
sudo mv /usr/lib/node_modules.bak/* /usr/lib/node_modules

# Remove old global `node_modules`
sudo rmdir /usr/lib/node_modules.bak