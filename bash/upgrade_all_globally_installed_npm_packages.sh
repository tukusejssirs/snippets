#!/usr/bin/env bash

# Upgrade all globally installed NPM packages
readarray -t packages <<< "$(npm list -g --depth 0 | grep '^[^/]' | sed 's/^[^ ]* \(.*\)@[0-9.]*$/\1/g')"
# shellcheck disable=SC2048,SC2086
sudo npm -g up ${packages[*]}