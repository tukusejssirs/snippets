#!/usr/bin/env bash

# Get a list of SMB shares on a particular host
nmap --script smb-enum-shares.nse -p445 --script-args smbusername=user,smbpassword=pass 192.168.1.1